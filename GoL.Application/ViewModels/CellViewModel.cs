﻿using System.Windows.Controls;

namespace GoL.Application.ViewModels
{
    public class CellViewModel : Button
    {
        public CellViewModel(int row, int column)
        {
            SetValue(Grid.RowProperty, row);
            SetValue(Grid.ColumnProperty, column);
        }

        protected override void OnClick()
        {
            Opacity = 1 - Opacity;
        }
    }
}