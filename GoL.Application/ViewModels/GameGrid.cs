﻿using System.Linq;
using System.Windows.Controls;
using GoL.Application.Models;

namespace GoL.Application.ViewModels
{
    public class GameGrid : Grid
    {
        public void Init(FieldModel fieldModel)
        {
            for (var i = 0; i < fieldModel.RowCount; i++)
                RowDefinitions.Add(new RowDefinition());
            for (var i = 0; i < fieldModel.ColumnCount; i++)
                ColumnDefinitions.Add(new ColumnDefinition());

            var cellHeight = Height / fieldModel.RowCount;
            var cellWidth = Width / fieldModel.ColumnCount;

            for (var row = 0; row < fieldModel.RowCount; row++)
            for (var column = 0; column < fieldModel.ColumnCount; column++)
            {
                var cellView = new CellViewModel(row, column)
                {
                    Height = cellHeight,
                    Width = cellWidth
                };
                Children.Add(cellView);
            }

            fieldModel.SetBinding(Children.OfType<CellViewModel>().ToArray());
        }
    }
}