﻿namespace GoL.Application
{
    public static class AppConsts
    {
        public const string StartTitle = "Start";
        public const string StopTitle = "Stop";
    }
}