﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using GoL.Core;

namespace GoL.Application.Models
{
    public class CellModel : DependencyObject, ICell
    {
        private static readonly DependencyProperty OpacityProperty =
            DependencyProperty.Register(nameof(OpacityProperty), typeof(int), typeof(CellModel));

        public bool IsAlive
        {
            get => (int) GetValue(OpacityProperty) == 1;
            set => SetValue(OpacityProperty, value ? 1 : 0);
        }

        public bool IsSurvives { get; set; }

        public void SetBinding(Button cellView)
        {
            var binding = new Binding
            {
                Source = this,
                Path = new PropertyPath(nameof(OpacityProperty)),
                Mode = BindingMode.TwoWay
            };
            cellView.SetBinding(UIElement.OpacityProperty, binding);
        }
    }
}