﻿using System;
using System.Collections.Generic;
using System.Linq;
using GoL.Application.Extensions;
using GoL.Application.ViewModels;
using GoL.Core;

namespace GoL.Application.Models
{
    public class FieldModel : Field<CellModel>
    {
        public FieldModel(int rowCountConst, int columnCountConst) : base(rowCountConst, columnCountConst)
        {
        }

        public void SetBinding(CellViewModel[] cellModels)
        {
            if (cellModels.Length != Source.Length)
                throw new ArgumentException(nameof(cellModels));

            var i = 0;
            ForEach(cell => cell.SetBinding(cellModels[i++]));
        }

        public byte[] ToByteArray()
            => Source.Cast<ICell>().Select(cell => cell.IsAlive).ToArray().ConvertBoolArrayToBytes().ToArray();

        public void FromBytes(IEnumerable<byte> source)
        {
            var boolArray = source.ConvertBytesToBoolArray();
            var i = 0;
            ForEach(cell => cell.IsAlive = i < boolArray.Length && boolArray[i++]);
        }
    }
}