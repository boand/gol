﻿using System.Collections.Generic;
using System.Linq;

namespace GoL.Application.Extensions
{
    public static class Converter
    {
        public static IEnumerable<byte> ConvertBoolArrayToBytes(this bool[] source)
        {
            for (var i = 0; i < source.Length; i += 8)
                yield return ConvertBoolArrayToByte(source.Skip(i).Take(8));
        }

        public static byte ConvertBoolArrayToByte(this IEnumerable<bool> source)
        {
            byte result = 0;

            var index = 7;
            foreach (var b in source)
            {
                if (b)
                    result |= (byte) (1 << index);

                index--;
            }

            return result;
        }

        public static bool[] ConvertBytesToBoolArray(this IEnumerable<byte> source)
        {
            return source.Select(ConvertByteToBoolArray).SelectMany(boolArray => boolArray).ToArray();
        }

        public static bool[] ConvertByteToBoolArray(byte b)
        {
            var result = new bool[8];

            for (var i = 0; i < 8; i++)
                result[i] = (b & (1 << (7 - i))) != 0;


            return result;
        }
    }
}