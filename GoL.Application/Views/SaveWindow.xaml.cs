﻿using System.Windows;

namespace GoL.Application.Views
{
    public partial class SaveWindow
    {
        public SaveWindow(string prevTitle)
        {
            InitializeComponent();
            if (!string.IsNullOrWhiteSpace(prevTitle))
                TitleBox.Text = prevTitle;
        }

        public string TitleText
            => TitleBox.Text;

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TitleBox.Text))
                MessageBox.Show("Title is empty", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            else
                DialogResult = true;
        }
    }
}