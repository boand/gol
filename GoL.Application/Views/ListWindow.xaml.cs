﻿using System.Linq;
using System.Windows;
using GoL.Application.Server;

namespace GoL.Application.Views
{
    public partial class ListWindow
    {
        public ListWindow(IStorageService storageService, string actionText)
        {
            InitializeComponent();
            Title = actionText;
            OkBtn.Content = actionText;

            TitleBox.ItemsSource = storageService.GetSavedTitles()
                .Where(title => title != AppConsts.StartTitle && title != AppConsts.StopTitle);
        }

        public string TitleText
            => TitleBox.Text;

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}