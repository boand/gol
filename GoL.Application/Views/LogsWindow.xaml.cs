﻿using GoL.Application.Server;

namespace GoL.Application.Views
{
    public partial class LogsWindow
    {
        public LogsWindow(IStorageService storageService)
        {
            InitializeComponent();
            LogList.ItemsSource = storageService.GetTitlesLogs(new[] {AppConsts.StartTitle, AppConsts.StopTitle});
        }
    }
}