﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using GoL.Application.Models;
using GoL.Application.Server;
using GoL.Core;

namespace GoL.Application.Views
{
    public partial class GameWindow
    {
        private readonly FieldModel _fieldModel;
        private readonly Game<CellModel> _game;
        private readonly IStorageService _storageService;
        private readonly int _timeDelay;

        public GameWindow()
        {
            InitializeComponent();

            _storageService = new StorageServiceClient();
            var gameSettings = _storageService.GetGameSettings();
            _timeDelay = gameSettings.TimeDelay;
            _fieldModel = new FieldModel(gameSettings.RowCount, gameSettings.ColumnCount);
            _game = new Game<CellModel>(_fieldModel);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GameGrid.Init(_fieldModel);
        }

        private void Random_OnClick(object sender, RoutedEventArgs e)
            => _game.InitRandom();

        private void NextStep_OnClick(object sender, RoutedEventArgs e)
            => _game.TryLifeStep();

        private void Clear_OnClick(object sender, RoutedEventArgs e)
            => _game.KillEveryone();

        private void Infinite_Checked(object sender, RoutedEventArgs e)
            => _fieldModel.Infinite = (sender as MenuItem)?.IsChecked == true;

        private async void Auto_Checked(object sender, RoutedEventArgs e)
        {
            var auto = (MenuItem) sender;
            var title = auto.IsChecked ? AppConsts.StartTitle : AppConsts.StopTitle;
            _storageService.SaveData(title, _fieldModel.ToByteArray());

            while (auto.IsChecked && _game.TryLifeStep())
                await Task.Delay(_timeDelay);

            if (!auto.IsChecked)
                return;

            auto.IsChecked = false;
            _storageService.SaveData(AppConsts.StopTitle, _fieldModel.ToByteArray());
        }

        private void Save_OnClick(object sender, RoutedEventArgs e)
        {
            var saveWindow = new SaveWindow(Title);

            if (saveWindow.ShowDialog() == true)
                if (_storageService.TrySaveData(saveWindow.TitleText, _fieldModel.ToByteArray()))
                    Title = saveWindow.TitleText;
                else
                    MessageBox.Show("Arrangement already saved", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void Load_OnClick(object sender, RoutedEventArgs e)
        {
            var listWindow = new ListWindow(_storageService, "Load");

            if (listWindow.ShowDialog() == true)
                _fieldModel.FromBytes(_storageService.GetSavedData(listWindow.TitleText));
        }

        private void RandomLoad_OnClick(object sender, RoutedEventArgs e)
            => _fieldModel.FromBytes(_storageService.GetRandomSavedData());

        private void Delete_OnClick(object sender, RoutedEventArgs e)
        {
            var listWindow = new ListWindow(_storageService, "Delete");

            if (listWindow.ShowDialog() == true)
                _storageService.DeleteSavedData(listWindow.TitleText);
        }

        private void Logs_OnClick(object sender, RoutedEventArgs e)
            => new LogsWindow(_storageService).ShowDialog();
    }
}