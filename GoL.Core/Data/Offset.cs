﻿namespace GoL.Core.Data
{
    internal class Offset
    {
        public static Offset[] AroundCell =
        {
            new Offset(-1, -1),
            new Offset(0, -1),
            new Offset(1, -1),

            new Offset(-1, 0),
            new Offset(1, 0),

            new Offset(-1, 1),
            new Offset(0, 1),
            new Offset(1, 1)
        };

        public Offset(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }

        public int Y { get; }
    }
}