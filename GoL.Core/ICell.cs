﻿namespace GoL.Core
{
    public interface ICell
    {
        bool IsAlive { get; set; }
        bool IsSurvives { get; set; }
    }
}