﻿using System;
using System.Linq;

namespace GoL.Core
{
    public class Game<T> where T : class, ICell, new()
    {
        private readonly Field<T> _field;
        private readonly Random _rnd = new Random();

        public Game(Field<T> field)
        {
            _field = field;
        }

        public void InitRandom()
            => _field.ForEach(cell => cell.IsAlive = _rnd.Next(42) < 10);

        public void KillEveryone()
            => _field.ForEach(cell => cell.IsAlive = false);

        public bool TryLifeStep()
        {
            if (_field.All(cell => !cell.IsAlive))
                return false;

            for (var i = 0; i < _field.RowCount; i++)
            for (var j = 0; j < _field.ColumnCount; j++)
            {
                var countLiveNeighbors = _field.GetNeighbors(i, j)
                    .Sum(offset => offset.IsAlive ? 1 : 0);
                _field[i, j].IsSurvives = CanSurvives(_field[i, j].IsAlive, countLiveNeighbors);
            }

            if (_field.All(cell => cell.IsAlive == cell.IsSurvives))
                return false;

            _field.ForEach(cell => cell.IsAlive = cell.IsSurvives);
            return true;
        }

        public bool CanSurvives(bool isAlive, int countLiveNeighbors)
            => isAlive
                ? countLiveNeighbors == 2 || countLiveNeighbors == 3
                : countLiveNeighbors == 3;
    }
}