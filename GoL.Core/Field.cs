﻿using System;
using System.Collections.Generic;
using System.Linq;
using GoL.Core.Data;

namespace GoL.Core
{
    public class Field<T> where T : new()
    {
        protected readonly T[,] Source;
        public bool Infinite;

        public Field(int rowCount, int columnCount)
        {
            Source = new T[rowCount, columnCount];

            for (var row = 0; row < rowCount; row++)
            for (var column = 0; column < columnCount; column++)
                Source[row, column] = new T();
        }

        public int RowCount
            => Source.GetLength(0);

        public int ColumnCount
            => Source.GetLength(1);

        public T this[int x, int y]
        {
            get => Source[x, y];
            set => Source[x, y] = value;
        }

        public void ForEach(Action<T> action)
        {
            foreach (var cell in Source)
                action(cell);
        }

        public bool All(Func<T, bool> predicate)
            => Source.Cast<T>().All(predicate);

        public bool Any(Func<T, bool> predicate)
            => Source.Cast<T>().Any(predicate);

        public IEnumerable<T> GetNeighbors(int row, int column)
        {
            var cells = Offset.AroundCell.Select(offset => new
            {
                x = row + offset.X,
                y = column + offset.Y
            });

            cells = Infinite
                ? cells.Select(cell
                    => new
                    {
                        x = (cell.x + RowCount) % RowCount,
                        y = (cell.y + ColumnCount) % ColumnCount
                    })
                : cells
                    .Where(cell => 0 <= cell.x && cell.x < RowCount)
                    .Where(cell => 0 <= cell.y && cell.y < ColumnCount);

            return cells.Select(cell => Source[cell.x, cell.y]);
        }
    }
}