﻿using System.Linq;
using GoL.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GoL.Tests
{
    [TestClass]
    public class GameTests
    {
        private Field<Mock> _field;
        private Game<Mock> _game;

        [TestInitialize]
        public void SetUp()
        {
            _field = new Field<Mock>(10, 10);
            _game = new Game<Mock>(_field);
        }

        [TestMethod]
        [DataRow(false, new[] {3})]
        [DataRow(true, new[] {2, 3})]
        public void CanSurvives(bool isAlive, int[] acceptableNeighbors)
        {
            for (var countLiveNeighbors = 0; countLiveNeighbors < 8; countLiveNeighbors++)
            {
                var isSurvives = _game.CanSurvives(isAlive, countLiveNeighbors);

                var willSurvives = acceptableNeighbors.Contains(countLiveNeighbors);
                Assert.AreEqual(willSurvives, isSurvives, $" Count Live Neighbors [{countLiveNeighbors}]");
            }
        }

        [TestMethod]
        public void InitRandom()
        {
            _game.InitRandom();

            Assert.AreEqual(true, _field.Any(cell => cell.IsAlive));
        }

        [TestMethod]
        public void KillEveryone()
        {
            _field[0, 0].IsAlive = true;

            _game.KillEveryone();

            Assert.AreEqual(false, _field.Any(cell => cell.IsAlive));
        }

        [TestMethod]
        public void AllDied()
        {
            _field.ForEach(cell => cell.IsAlive = false);

            var step = _game.TryLifeStep();

            Assert.AreEqual(false, step);
        }

        [TestMethod]
        public void NothingChanged()
        {
            _field[1, 1].IsAlive = true;
            _field[1, 2].IsAlive = true;
            _field[2, 1].IsAlive = true;
            _field[2, 2].IsAlive = true;
            var previous = Clone(_field);

            var step = _game.TryLifeStep();

            Assert.AreEqual(false, step);
            AssertFieldsAreEqual(previous, _field);
        }

        [TestMethod]
        public void Blinker()
        {
            _field[0, 1].IsAlive = true;
            _field[1, 1].IsAlive = true;
            _field[2, 1].IsAlive = true;

            var step = _game.TryLifeStep();

            Assert.AreEqual(true, step);
            var etalon = new Field<Mock>(10, 10)
            {
                [1, 0] = {IsAlive = true},
                [1, 1] = {IsAlive = true},
                [1, 2] = {IsAlive = true}
            };
            AssertFieldsAreEqual(etalon, _field);
        }

        private void AssertFieldsAreEqual(Field<Mock> copy, Field<Mock> field)
        {
            for (var i = 0; i < copy.RowCount; i++)
            for (var j = 0; j < copy.ColumnCount; j++)
                Assert.AreEqual(copy[i, j].IsAlive, field[i, j].IsAlive, $" Wrong in [{i}, {j}]");
        }

        private Field<Mock> Clone(Field<Mock> field)
        {
            var copy = new Field<Mock>(field.RowCount, field.ColumnCount);

            for (var i = 0; i < copy.RowCount; i++)
            for (var j = 0; j < copy.ColumnCount; j++)
                copy[i, j].IsAlive = field[i, j].IsAlive;

            return copy;
        }

        private class Mock : ICell
        {
            public bool IsAlive { get; set; }
            public bool IsSurvives { get; set; }
        }
    }
}