﻿using System.Linq;
using GoL.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GoL.Tests
{
    [TestClass]
    public class FieldTests
    {
        private Field<Mock> _field;

        [TestInitialize]
        public void SetUp()
        {
            _field = new Field<Mock>(10, 10);
        }

        [TestMethod]
        [DataRow(0, 0)]
        [DataRow(9, 0)]
        [DataRow(0, 9)]
        [DataRow(9, 9)]
        public void CountNeighbors(int row, int column)
        {
            _field.Infinite = false;

            Assert.AreEqual(3, _field.GetNeighbors(row, column).Count());
        }

        [TestMethod]
        [DataRow(0, 0)]
        [DataRow(9, 0)]
        [DataRow(0, 9)]
        [DataRow(9, 9)]
        public void CountNeighborsForInfinite(int row, int column)
        {
            _field.Infinite = true;

            Assert.AreEqual(8, _field.GetNeighbors(row, column).Count());
        }

        private class Mock
        {
        }
    }
}