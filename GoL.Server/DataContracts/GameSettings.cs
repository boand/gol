﻿using System.Runtime.Serialization;

namespace GoL.Server.DataContracts
{
    [DataContract]
    public class GameSettings
    {
        [DataMember] public int RowCount { get; set; } = 20;
        [DataMember] public int ColumnCount { get; set; } = 30;
        [DataMember] public int TimeDelay { get; set; } = 200;
    }
}