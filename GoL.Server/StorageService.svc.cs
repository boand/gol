﻿using System;
using System.Linq;
using GoL.Server.Contexts;
using GoL.Server.DataContracts;
using GoL.Server.Domains;

namespace GoL.Server
{
    public class StorageService : IStorageService, IDisposable
    {
        private readonly StorageContext _storageContext;

        public StorageService()
        {
            _storageContext = new StorageContext();
        }

        public void Dispose()
        {
            _storageContext?.Dispose();
        }

        public GameSettings GetGameSettings()
        {
            var settings = _storageContext.Settings.ToArray();
            var gameSettings = new GameSettings
            {
                RowCount = settings.Single(x => x.Type == SettingsType.RowCount).Value,
                ColumnCount = settings.Single(x => x.Type == SettingsType.ColumnCount).Value,
                TimeDelay = settings.Single(x => x.Type == SettingsType.TimeDelay).Value
            };
            return gameSettings;
        }

        public void SaveData(string title, byte[] content)
        {
            _storageContext.SavedData.Add(new SavedData
            {
                Title = title,
                Date = DateTime.Now,
                Content = content
            });

            _storageContext.SaveChanges();
        }

        public bool TrySaveData(string title, byte[] content)
        {
            var lastSaved = _storageContext.SavedData.GroupBy(x => x.Title)
                .Select(grouping => grouping.OrderByDescending(x => x.Date).FirstOrDefault());

            if (lastSaved.Any(x => x.Content == content))
                return false;

            SaveData(title, content);
            return true;
        }

        public string[] GetSavedTitles()
            => _storageContext.SavedData.Select(x => x.Title).Distinct().ToArray();

        public string[] GetTitlesLogs(string[] titles)
            => _storageContext.SavedData.Where(x => titles.Contains(x.Title)).OrderByDescending(x => x.Date).Take(10)
                .ToArray().Select(x => $"{x.Title} - {x.Date}").ToArray();

        public byte[] GetSavedData(string title)
            => _storageContext.SavedData.OrderByDescending(x => x.Date).FirstOrDefault(x => x.Title == title)
                   ?.Content ?? new byte[0];

        public byte[] GetRandomSavedData()
            => _storageContext.SavedData.OrderBy(_ => Guid.NewGuid()).First().Content;

        public void DeleteSavedData(string title)
        {
            var savedData = _storageContext.SavedData.Where(x => x.Title == title);
            _storageContext.SavedData.RemoveRange(savedData);
            _storageContext.SaveChanges();
        }
    }
}