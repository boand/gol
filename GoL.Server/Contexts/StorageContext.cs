﻿using System.Data.Entity;
using GoL.Server.Domains;

namespace GoL.Server.Contexts
{
    public class StorageContext : DbContext
    {
        static StorageContext()
        {
            Database.SetInitializer(new ContextInitializer());
        }

        public DbSet<SavedSettings> Settings { get; set; }
        public DbSet<SavedData> SavedData { get; set; }
    }
}