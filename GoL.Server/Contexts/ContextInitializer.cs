﻿using System.Data.Entity;
using GoL.Server.Domains;

namespace GoL.Server.Contexts
{
    public class ContextInitializer : CreateDatabaseIfNotExists<StorageContext>
    {
        protected override void Seed(StorageContext db)
        {
            db.Settings.Add(new SavedSettings {Type = SettingsType.RowCount, Value = 20});
            db.Settings.Add(new SavedSettings {Type = SettingsType.ColumnCount, Value = 30});
            db.Settings.Add(new SavedSettings {Type = SettingsType.TimeDelay, Value = 200});
            db.SaveChanges();
        }
    }
}