﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoL.Server.Domains
{
    public class SavedSettings
    {
        [Key]
        [Column(nameof(Type))]
        public string TypeString
        {
            get => Type.ToString();
            set => Type = (SettingsType) Enum.Parse(typeof(SettingsType), value, true);
        }

        [NotMapped] public SettingsType Type { get; set; }

        public int Value { get; set; }
    }
}