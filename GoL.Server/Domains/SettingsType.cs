﻿namespace GoL.Server.Domains
{
    public enum SettingsType
    {
        RowCount,
        ColumnCount,
        TimeDelay
    }
}