﻿using System;

namespace GoL.Server.Domains
{
    public class SavedData
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public byte[] Content { get; set; }
    }
}