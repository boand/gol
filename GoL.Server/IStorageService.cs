﻿using System.ServiceModel;
using GoL.Server.DataContracts;

namespace GoL.Server
{
    [ServiceContract]
    public interface IStorageService
    {
        [OperationContract]
        GameSettings GetGameSettings();

        [OperationContract]
        void SaveData(string title, byte[] content);

        [OperationContract]
        bool TrySaveData(string title, byte[] content);

        [OperationContract]
        string[] GetSavedTitles();

        [OperationContract]
        string[] GetTitlesLogs(string[] titles);

        [OperationContract]
        byte[] GetSavedData(string title);

        [OperationContract]
        byte[] GetRandomSavedData();

        [OperationContract]
        void DeleteSavedData(string title);
    }
}